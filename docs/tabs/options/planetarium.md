The Planetarium tab contains settings for each of the 4 supported planetarium programs.
Currently N.I.N.A. supports Stellarium, Cartes du Ciel, TheSkyX and HNSKY.
The connection allows a one way communication of coordaintes from the planetarium software to N.I.N.A. 

If a planetarium program is configured, coordinates can be imported anywhere in the program that has the Planetarium Sync Button.

![The planetarium options tab](../../images/tabs/planetariumSettings1.png)

1. **Preferred Planetarium Software**
    * This drop down menu selects the planetarium software to be used

2. **Host**
    * This is the address the planetarium server is hosted on
    > The default 'localhost' will work if you're running the planetarium software on the same machine

3. ** Port**
    * Each software's server operates on a different port
    > It is recommended to leave this as is
    
4. **Timeout**
    * The time to wait for a request response in milliseconds